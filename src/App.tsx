import React from "react";
import "./App.css";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import Photosets from "./pages/Photosets";
import Header from "./components/Header";
import Collections from "./pages/Collections";
import { FlickrContextProvider } from "./context/flickr/FlickrContext";

function App() {
  return (
    <FlickrContextProvider>
      <Router>
        <Container>
          <Header />
          <main>
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
              <Route path="/photosets">
                <Photosets />
              </Route>
              <Route path="/collections">
                <Collections />
              </Route>
            </Switch>
          </main>
        </Container>
      </Router>
    </FlickrContextProvider>
  );
}

export default App;
