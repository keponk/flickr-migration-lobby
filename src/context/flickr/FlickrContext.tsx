import React, { createContext, useContext, useReducer, ReactNode } from "react";
import { IState, ActionType, FlickrContextType } from "../FlickrTypes";
import flickrReducer from "./FlickrReducer";
import axios, { AxiosResponse } from "axios";

const FlickrContext = createContext<FlickrContextType>({} as FlickrContextType);

type Props = {
  children: ReactNode;
};

export const FlickrContextProvider = (props: Props) => {
  const initialState: IState = {
    nsid: "",
    username: "",
    photoCount: 0,
    albumCount: 0,
    collectionCount: 0,
  };

  const [state, dispatch] = useReducer(flickrReducer, initialState);

  const getHomeData = async (): Promise<any> => {
    try {
      let res: AxiosResponse = await axios.get("/api/v1/flickr/summary");
      console.log(res.data);
      dispatch({
        type: ActionType.GET_USER_DATA,
        payload: res.data,
      });
    } catch (error) {
      console.error(error);
    }
  };

  const value: FlickrContextType = {
    nsid: state.nsid,
    username: state.username,
    photoCount: state.photoCount,
    albumCount: state.albumCount,
    collectionCount: state.collectionCount,
    getHomeData,
  };

  return (
    <FlickrContext.Provider value={value}>
      {props.children}
    </FlickrContext.Provider>
  );
};

export const useFlickrContext = () => {
  const context: FlickrContextType = useContext(FlickrContext);

  if (context) {
    // TO-DO: check TS-way of throwing errors
    return context;
  }
};
