import { IAction, IState, ActionType } from "../FlickrTypes";

export default (state: IState, action: IAction): IState => {
  switch (action.type) {
    case ActionType.GET_USER_DATA:
      return {...action.payload};
    default:
      return state;
  }
};
