export interface IState {
  nsid: string;
  username: string;
  photoCount: number;
  albumCount: number;
  collectionCount: number;
}

export enum ActionType {
  "GET_USER_DATA",
}

export interface IAction {
  type: ActionType;
  payload: any;
}

export interface IReducerProps {
  state: IState;
  dispatch: ({ type }: { type: string }) => void;
}

export type FlickrContextType = {
  nsid: string;
  username: string;
  photoCount: number;
  albumCount: number;
  collectionCount: number;
  getHomeData: () => void;
};
