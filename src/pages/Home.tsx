import React, { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import { FlickrContextType } from "../context/FlickrTypes";
import { useFlickrContext } from "../context/flickr/FlickrContext";

const Home = () => {
  const {
    username,
    photoCount,
    albumCount,
    collectionCount,
    getHomeData,
  }: FlickrContextType = useFlickrContext()!;

  useEffect(() => {
    getHomeData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const [input, setInput] = useState("");

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
    e.preventDefault();
    setInput(e.target.value);
  };

  const handleOnSubmit = (e: React.FormEvent): void => {
    e.preventDefault();
    console.log("form submit handler");
    getHomeData();
    setInput("");
  };

  return (
    <>
      <div className="user-data">
        <Form onSubmit={handleOnSubmit}>
          <Form.Group controlId="ControlInput1">
            <Form.Control
              autoFocus
              value={input}
              onChange={handleInputChange}
              className="ControlInput1"
              type="text"
              placeholder="just a simple placeholder"
            />
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit
          </Button>
        </Form>
        <div>
          <span>
            <a href="/">User: </a>
          </span>
          <span className="data-username">{username}</span>
          <br />
          <span>Photos: </span>
          <span className="data-photoCount">{photoCount}</span>
          <br />
          <span>
            <a href="/photosets">Photosets(albums): </a>
          </span>
          <span className="data-albumCount">{albumCount}</span>
          <br />
          <span>Collections: </span>
          <span className="data-collectionCount">{collectionCount}</span>
          <br />
          <p>Hi</p>
        </div>
      </div>
    </>
  );
};

export default Home;
