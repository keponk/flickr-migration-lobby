import React from "react";

const Collections = () => {
  const collectionsArray = [
    { id: 1, title: "asdd", set: [] },
    { id: 2, title: "srtfsdfdf", set: [] },
    { id: 3, title: "sdftgesger", set: [] },
  ];
  return (
    <>
      {collectionsArray.map((collection) => (
        <div key={collection.id}>
          <ul>
            <li>{collection.id}</li>
            <li>{collection.title}</li>
          </ul>
        </div>
      ))}
    </>
  );
};

export default Collections;
