export {};

declare module FlickrPhoto {
  export interface Dates {
    posted: string;
    taken: string;
    takengranularity: string;
    takenunknown: string;
    lastupdate: string;
  }

  export interface RootObject {
    id: string;
    originalformat: string;
    title: string;
    dates: Dates;
    url: string;
    media: string[];
  }
}
