import React from "react";
import { Navbar, Nav } from "react-bootstrap";
import { BrowserRouter as Router } from "react-router-dom";

const Header = () => {
  return (
    <header>
      <Router>
        <Navbar>
          <Navbar.Brand href="/">Flickr Migration Lobby</Navbar.Brand>

          <Nav.Link href="/">Home</Nav.Link>

          <Nav.Link href="/photosets">Photosets</Nav.Link>
          <Nav.Link href="/collections">Collections</Nav.Link>
          <Nav.Link href="#home">Option 2</Nav.Link>
        </Navbar>
      </Router>
    </header>
  );
};

export default Header;
