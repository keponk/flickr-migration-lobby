describe("App initialization", () => {
  it("Loads user info", () => {
    cy.seedAndVisit();

    cy.get(".data-username").should("not.equal", "");
    cy.get(".data-photoCount").should("not.equal", "");
    cy.get(".data-albumCount").should("not.equal", "");
    cy.get(".data-collectionCount").should("not.equal", "");
  });
});
