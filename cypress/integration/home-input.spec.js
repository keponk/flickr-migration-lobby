describe("Data loader", () => {
  beforeEach(() => {
    // cy.visit("/");
    cy.seedAndVisit([]);
  });

  it("has data", () => {
    const textToBeTyped = "Hello  Test";

    cy.focused().should("have.class", "ControlInput1");

    cy.get(".ControlInput1")
      .type(textToBeTyped, { delay: 100 })
      .should("have.value", textToBeTyped);
  });

  context("all form stuff", () => {
    it("Adds new item to state", () => {
      cy.server();
      cy.route("POST", "/api/v1/flickr/photo", {
        nsid: "12312",
        username: "some_user_name",
        photoCount: 120,
        albumCount: 132,
        collectionCount: 12,
      });

      cy.get(".ControlInput1")
        .type("adding another text string", { delay: 100 })
        .type("{enter}")
        .should("have.value", "");

      // cy.get(".ControlInput1")
      //   .type("adding another text string")
      //   .type("{enter}")
      //   .should("have.value", "");

      cy.get(".data-photoCount").contains(120);
    });
  });
});
